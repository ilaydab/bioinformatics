#!/bin/bash

# module load supernova/2.1.1
# module load nixpkgs/16.09  gcc/7.3.0
# module load freebayes/1.2.0
# module load picard

lr=/lustre04/scratch/ilaydab/longranger-2.2.2/
dm=/home/ilaydab/scratch/buk-2-compare/DM/potato_dm_v404_all_pm_un.fasta


${lr}/longranger mkref $dm

java -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=refdata-potato_dm_v404_all_pm_un/fasta/genome.fa O=refdata-potato_dm_v404_all_pm_un/fasta/genome.dict

${lr}/longranger wgs \
	--id=x-l-buk-2 \
	--fastqs=${ref}/CIP_BUK_ATTGGCGT,${ref}/CIP_BUK_CGCCATCG,${ref}/CIP_BUK_GCAACAAA,${ref}/CIP_BUK_TAGTTGTC,${ref1}/NDHX00513-AK3468,${ref1}/NDHX00513-AK3469,${ref1}/NDHX00513-AK3470,${ref1}/NDHX00513-AK3471 \
	--sample=CIP_BUK_ATTGGCGT,CIP_BUK_CGCCATCG,CIP_BUK_GCAACAAA,CIP_BUK_TAGTTGTC,NDHX00513 \
    --reference=/home/ilaydab/scratch/buk-2-compare/DM/refdata-potato_dm_v404_all_pm_un \
	--vcmode=freebayes \
	--sex=female
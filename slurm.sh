#!/bin/bash

#SBATCH --account=def-mstrom
#SBATCH --time=2:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=16
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=10G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ilayda.bozan@mail.mcgill.ca
#SBATCH --job-name="BUK2 Decontamination Attempt MUMMER"
#SBATCH --output=output.%j.txt

# Set working directory
WORKDIR=/home/ilaydab/scratch/BUK-2/remove-chloroplast
cd ${WORKDIR}
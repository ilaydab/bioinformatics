library(dplyr)
library(PtProcess)
library(ggplot2)
library(gtable)
library(grid)
library(gridExtra)


##GENERAL SNP RESULTS
snps <- read.csv("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/SNP-results.csv", header=T, as.is=T, sep=",")

depth.cov <- table(snps$Depth,snps$Covered...of.Bases.of.Ref.Genome)
row.names(depth.cov)<-snps$X

p1 <- ggplot(data=snps, aes(x=snps$X, y=snps$Depth, fill=snps$X)) + geom_bar(stat="identity") + geom_text(aes(label=round(snps$Depth)), vjust=0.1, size=5) + theme_minimal()
p1 + coord_flip() + ylab("Depth of Coverage") + xlab("Alignment") + scale_fill_discrete(name = "Alignment") 


p2 <- ggplot(data=snps, aes(x=snps$X, y=snps$Covered...of.Bases.of.Ref.Genome/1000000, fill=snps$X)) + geom_bar(stat="identity") + geom_text(aes(label=round(snps$Covered...of.Bases.of.Ref.Genome/1000000)), vjust=-0.5, size=5) + theme_minimal()
p2 + coord_flip()  + xlab(" ") + ylab("Reference Genome Coverage in Mb") + scale_fill_discrete(name = "Alignments") 


p3 <- ggplot(data=snps, aes(x=snps$X, y=snps$X..of.SNPS, fill=snps$X)) + geom_bar(stat="identity") + geom_text(aes(label=snps$X..of.SNPS), vjust=-0.1, size=8) + theme_minimal()
p3 + coord_flip()  + xlab(" ") + ylab("Number of SNPs") + scale_fill_discrete(name = "Alignments") 


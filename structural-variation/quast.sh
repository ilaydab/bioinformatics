module load nixpkgs/16.09
module load python/3.7.4

python -m pip install -U matplotlib

buk=${WORKDIR}/BUK-2-CLEAN.fasta
reference=/home/ilaydab/scratch/buk-2-DM/PGCS/dm-12chroms.fa
genes=/home/ilaydab/scratch/decontamination/PGCS_DM_V403_renamed_only_genes.gff
reads1=/home/ilaydab/scratch/buk2-rawdata/CIP_BUK2_10xGenomics_read.1.fq
reads2=/home/ilaydab/scratch/buk2-rawdata/CIP_BUK2_10xGenomics_read.2.fq

/home/ilaydab/scratch/quast/quast.py ${buk} \
        -r ${reference} \
        -g ${genes} \
        -1 ${reads1} -2 ${reads2} \
        -o quast_output

#!/bin/bash

WORKDIR=/home/ilaydab/scratch/buk-2-compare/DM
cd ${WORKDIR}

#Names of the alinies
# NOTE: RUN THE SCTRIPT
ALN_1=BUK2
ALN_2=DM
assembly=/home/ilaydab/scratch/buk-2-compare/DM/potato_dm_v404_all_pm_un.fasta

#Necessary Files for the process
#read1=${ALN_1}-read1.fastq
#read2=${ALN_1}-read2.fastq

reads1=/home/ilaydab/scratch/buk2-rawdata/CIP_BUK2_10xGenomics_read.1.fq
reads2=/home/ilaydab/scratch/buk2-rawdata/CIP_BUK2_10xGenomics_read.2.fq
reads=/home/ilaydab/scratch/longranger-results/x-l-longranger/outs/reads.fq.gz

#Files that will be created
aln1aln2=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}.bam
sortedaln1aln2=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}-sorted.bam
markedDupaln1aln2=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}-sorted-markedDuplicates.bam
markedDupSortedaln1aln2=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}-sorted-markedDuplicates-sorted.bam
properlyoriented=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}-sorted-markedDuplicates-sorted-properly-oriented.bam
alignedproperlyoriented=${WORKDIR}/${ALN_1}-final-assembly-${ALN_2}-sorted-markedDuplicates-sorted-properly-oriented-only-aligened.bam
finalAlignementManuplation=${WORKDIR}/_${ALN_1}-${ALN_2}-alignment-manuplated-final.bam
#Variables for SNP
var1=${ALN_1}-${ALN_2}-sorted-var.vcf
var2=${ALN_1}-${ALN_2}-sorted-filtered-var.vcf
var3=${ALN_1}-${ALN_2}-sorted-filtered-annotated-var.vcf

#Modules
module load bwa/0.7.17
module load java/1.8.0_192
module load samtools/1.10
module load picard/2.18.9 
#Binaries
snpeff=/home/ilaydab/scratch/snpEff
freebayes=/home/ilaydab/scratch/freebayes-v1.3.1

#---------------- ALIGNMENT -----------------------------------------------------------
#bwa index ${assembly}

#For regular paired end reads

#bwa mem -t 16 ${assembly} ${read1} ${read2} | samtools sort -o ${aln1aln2} -
#samtools sort ${aln1aln2} -o ${sortedaln1aln2}

#For 10x Reads
bwa mem -t 20 -p -C $assembly $reads | samtools sort -@8 -tBX -o ${aln1aln2} -


#Sort and index the alignment
samtools index ${sortedaln1aln2}


#---------------- ALIGNMENT MANUPLATION ------------------------------------------------
#Mark Duplicates
java -jar $EBROOTPICARD/picard.jar MarkDuplicates \
	I=${sortedaln1aln2} \
	O=${markedDupaln1aln2} \
	M=${WORKDIR}/metrics-mark-dup-${ALN_1}-${ALN_2}-final-assembly.txt

#Sort and index the alignment
samtools sort ${markedDupaln1aln2} -o ${markedDupSortedaln1aln2}
samtools index ${markedDupSortedaln1aln2}

#Remove unproperly oriented reads & keep only aligned reads
samtools view -bhf 2 ${markedDupSortedaln1aln2} -o ${properlyoriented}
samtools view -b -F 4 ${properlyoriented} -o ${alignedproperlyoriented}

#Rename the last alignment file
cp ${alignedproperlyoriented} ${finalAlignementManuplation}

#---------------- ALIGNMENT SNP ANALYSIS ---------------------------------------------
#Modules for SNP
module load nixpkgs/16.09  gcc/7.3.0
module load vcflib/1.0.1

#Calling SNPs
${freebayes} -f ${assembly} ${alignment} > ${var1}

#SNP filtering
vcffilter -f "DP > 4 & QUAL > 20 & MQM > 20 & MQMR > 20 & SAF > 0 & SAR > 0" ${var1} > ${var2}

module load java/13.0.1
cd /home/ilaydab/scratch/snpEff

java -Xmx4g -jar snpEff.jar -v -stats ${ALN_1}-${ALN_2}.html Solanum_tuberosum ${var2} > ${var3}
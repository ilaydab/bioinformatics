#!/bin/bash

#DM
##Depth of coverage -prints out the actual depth.
samtools depth  ${bamDM} | awk '{sum+=$3} END { print "DM Average = ",sum/NR}'
##How much of the genome is covered by the alignment.
bedtools genomecov -ibam ${bamDM} -g ${DMfasta} -bga | awk '$4>0 {bpCountNonZero+=($3-$2)} {print bpCountNonZero}' | tail -1 > ${results}/DM.genomecov.txt
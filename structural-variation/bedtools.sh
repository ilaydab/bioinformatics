fasta=
out=
bed=

#To mask the interval given in a bed file with N
bedtools maskfasta -fi ${fasta} -bed ${bed} -fo ${out}
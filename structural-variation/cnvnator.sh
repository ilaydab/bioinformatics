###########################
# Global Variables
rootFileName="GON1-and-BUK2-20200101-18.root"
#chroms="gon1_pseudo_01"
chroms="gon1_pseudo_01 gon1_pseudo_02 gon1_pseudo_03 gon1_pseudo_04 gon1_pseudo_05 gon1_pseudo_06 gon1_pseudo_07 gon1_pseudo_08 gon1_pseudo_09 gon1_pseudo_10 gon1_pseudo_11 gon1_pseudo_12"
bamFile="sorted-marked-dup-buk-2-gon-1.bam"

###########################

cd /home/ilaydab/scratch/buk-2-gon1

echo "This file tries the PROCESSED bam file with 30 bin size"
###########################
# Modules
module load nixpkgs/16.09
module load gcc/8.3.0
module load cnvnator/0.4.1
module load root/6.14.04
###########################


###########################
# Start Main task

echo "Starting: Extract read mapping"
# Extract read mapping 
cnvnator \
	-root ${rootFileName} \
	-tree ${bamFile} \
	-chrom ${chroms}

echo "Starting: Generate histogram"
# Generate histogram
cnvnator \
	-root ${rootFileName} \
	-his 300 \
	-chrom ${chroms}
	# -fasta gon1_assembled_potato.fa

echo "Starting: Calculate statistics"
# Calculate statistics
cnvnator \
	-root ${rootFileName} \
	-chrom ${chroms} \
	-stat 300

echo "Starting: Partition"
# Partition
cnvnator \
	-root ${rootFileName} \
	-chrom ${chroms} \
	-partition 300

echo "Starting: Call CNVs"
# Call CNVs
cnvnator \
	-root ${rootFileName} \
	-chrom ${chroms} \
	-call 300 > ${rootFileName}.out
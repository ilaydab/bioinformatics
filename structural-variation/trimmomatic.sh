#!/bin/bash

module load trimmomatic/0.36

INFILE=/home/ilaydab/scratch/buk-1-gon1
OUTFILE=/home/ilaydab/scratch/buk-1-gon1

java -jar /home/ilaydab/scratch/Trimmomatic-0.39/trimmomatic-0.39.jar PE \
${INFILE}/forward-paired-end.fastq.gz \
${INFILE}/reverse-paired-end.fastq.gz \
${OUTFILE}/trimmed-forward-paired.fastq.gz \
${OUTFILE}/trimmed-forward-unpaired.fastq.gz \
${OUTFILE}/trimmed-reverse-paired.fastq.gz \
${OUTFILE}/trimmed-reverse-unpaired.fastq.gz \
ILLUMINACLIP:/home/ilaydab/scratch/Trimmomatic-0.39/adapters/all-adapter-sequences.fa:2:40:15 \
LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
library(dplyr)
library(PtProcess)
library(ggplot2)
library(gtable)
library(grid)
library(gridExtra)



#Read the file

#Read BUK2-DM
buk2dm.cnv= read.delim("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/out/DM-BUK2-Arsen-20200101-02.root.out", header=F, sep = "\t",check.names = F, as.is = T)

m6commersonii.cnv = read.delim("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-M6-Results/M6-and-BUK2-without-samtools.out", header=F, sep = "\t",check.names = F, as.is = T)

colnames(m6commersonii.cnv) <- c("CNV_type", "coordinates", "CNV_size", "normalized_RD", "p-val1", "p-val2", "p-val3", "p-val4", "q0")
colnames(buk2dm.cnv) <- c("CNV_type", "coordinates", "CNV_size", "normalized_RD", "p-val1", "p-val2", "p-val3", "p-val4", "q0")

#Filter according to SV's bigger than 1000bp, P-val <= 0.01, q0 <0.5
filtered.cnv = filter(buk2dm.cnv, buk2dm.cnv[,3] >= 1000, buk2dm.cnv[,5] <= 0.01, buk2dm.cnv[,9] <= 0.5)

filtered.cnv.buk2.dm = filter(m6commersonii.cnv, m6commersonii.cnv[,3] >= 1000, m6commersonii.cnv[,5] <= 0.01, m6commersonii.cnv[,9] <= 0.5)

#Separate deletion and duplication

deletion =  filter(filtered.cnv, filtered.cnv[,1]=="deletion")
duplication = filter(filtered.cnv, filtered.cnv[,1]=="duplication")

max(deletion$CNV_size, na.rm=FALSE)
max(duplication$CNV_size, na.rm=FALSE)
mean(filtered.cnv$CNV_size, na.rm=FALSE)
median(filtered.cnv$CNV_size, na.rm=FALSE)

#### ANNOTATION
library(intansv)
gff <- read.table("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/PGSC_DM_V403_representative_genes.gff", header=F, as.is=T, sep = "\t")
gff2 <- gff[,c(1,3,4,5,7,9)]
colnames(gff2)<-c("chr","tag","start","end","strand","ID")
head(gff2)



x <- readCnvnator("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/out", method="CNVnator")
str(x)
y <- llply(x, svAnnotation, genomeAnnotation=gff2)
write.table(y$del, "/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/buk-2-DM-annotated-del")
write.table(y$dup, "/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/buk-2-DM-annotated-dup")

del.anno <-y$del
del.anno.first.three.col<- del.anno[,1:3]
dup.anno <-y$dup
dup.anno.first.three.col<- dup.anno[,1:3]


head(y$del)

#### VISUALISATION


size <- read.table("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-M6-Results/genome-sizes.txt", header=T, as.is=TRUE, sep = "\t")
colnames(size)<-make.names(size[1,], unique = TRUE)

plotChromosome(size, x,1000)

################################################

library("RCircos")
library(ggbio)
library(intansv)
library(rtracklayer)
library(circlize)
require(png)

png(filename="genome.CNV.png")
    
df=data.frame(name=c('ST4.03ch01', 'ST4.03ch02', 'ST4.03ch03','ST4.03ch04','ST4.03ch05','ST4.03ch06','ST4.03ch07','ST4.03ch08','ST4.03ch09','ST4.03ch10','ST4.03ch11','ST4.03ch12'),start=c(1,1,1,1,1,1,1,1,1,1,1,1),end = c(88663952,48614681,62290286,72208621,52070158,59532096,56760843,56938457,61540751,59756223,45475667,61165649),stringsAsFactors=FALSE)
circos.genomicInitialize(df,plotType=c("labels","axis"))

gene1<-read.delim("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-DM-Results/pgsc-genes.bed",header=F,row.names=NULL, sep='\t', as.is=T)
circos.genomicDensity(gene1,col = c("#FF000080"), track.height = 0.1)

#del <- read.table("ajh_del_annotated_genes.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(y$del,col = c("#3498DB"), track.height = 0.1)

#dup <- read.table("ajh_dup_annotated_genes.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(y$dup,col = c("#A93226"), track.height = 0.1)

dev.off()


###############################################################################################################################################################################
#DELETIONS

df=data.frame(name=c('ST4.03ch12'),start=(1),end = (61165649),stringsAsFactors=FALSE)
circos.genomicInitialize(df,plotType=c("labels","axis"))

gene1<-read.table("~/Desktop/Potato/Chr12_Total_gene_sort.bed",header=F,row.names=NULL, as.is=T)
circos.genomicDensity(gene1,col = c("#0000FF80"), track.height = 0.075)

del_phu <- read.table("~/Desktop/Potato/phu/chr12/phu_delinchr12_CNV_genes.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_phu,col = c("#3498DB"), track.height = 0.075)

del_gon1 <- read.table("gon1_chr12CNVdeletion_gene.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_gon1,col = c("#FF000080"), track.height = 0.075)

del_gon2 <- read.table("gon2_chr12_del_gene.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_gon2,col = c("#66C2A5"), track.height = 0.075)

del_ajh <- read.table("~/Desktop/Potato/ajh/chr12/ajh_chr12_del_annotated_genes.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_ajh,col = c("#FC8D62"), track.height = 0.075)

del_buk <- read.table("buk-CH12-del-sort.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_ajh,col = c("#A93226"), track.height = 0.075)

del_stn <- read.table("stn_chr12_del_annotated_gene.bed", row.names = NULL, as.is = T, header = F)
circos.genomicDensity(del_stn,col = c("#8DA0CB"), track.height = 0.075)

##############################################################################################################################################################################
######### M6 CNV analysis
##############################################################################################################################################################################

#png(filename="genome.CNV.png")
df=data.frame(name=c('M6_v4.1_chr01', 'M6_v4.1_chr02', 'M6_v4.1_chr03', 'M6_v4.1_chr04', 'M6_v4.1_chr05', 'M6_v4.1_chr06', 'M6_v4.1_chr07', 'M6_v4.1_chr08', 'M6_v4.1_chr09', 'M6_v4.1_chr10', 'M6_v4.1_chr11', 'M6_v4.1_chr12'),start=c(1,1,1,1,1,1,1,1,1,1,1,1),end = c(66849660,42376642,47120749,38708216,42711019,41715865,41467633,31643503,35384898,35706090,38261750,49944156),stringsAsFactors=FALSE)
circos.genomicInitialize(df,plotType=c("labels","axis"))
gene2<-read.table("/Users/ilayda/Desktop/McGill\ 260871108/Thesis/BUK2-M6-Results/sorted-genes.bed",header=F,row.names=NULL, as.is=T)
circos.genomicDensity(gene2,col = c("#FF000080"), track.height = 0.1)
circos.genomicDensity(y$del,col = c("#3498DB"), track.height = 0.1)
circos.genomicDensity(y$dup,col = c("#A93226"), track.height = 0.1)
#dev.off()

#!/bin/bash

# module load nixpkgs/16.09  gcc/7.3.0
# module load mummer/4.0.0beta2
# module load samtools/1.10

aln1=
aln2=
prefix=

# This is the actual process
nucmer -c 100 -p ${prefix} ${aln1} ${aln2}
# To see the coordinates of the alignment
show-coords -r -c -l ${prefix}.delta > ${prefix}.coords
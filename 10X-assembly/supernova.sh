#!/bin/bash

#module load supernova/2.1.1

library1=/lustre04/scratch/ilaydab/lJvVHi/data_release/C202SC18121578/raw_data/CIP_BUK_ATTGGCGT
library2=/lustre04/scratch/ilaydab/xV7Tms/data_release/C202SC18121578/raw_data

supernova run \
	--id=x-l-libraries \
	--fastqs=${library2}/CIP_BUK_ATTGGCGT,${library2}/CIP_BUK_CGCCATCG,${library2}/CIP_BUK_GCAACAAA,${library2}/CIP_BUK_TAGTTGTC,${library1}/NDHX00513-AK3468,${library1}/NDHX00513-AK3469,${library1}/NDHX00513-AK3470,${library1}/NDHX00513-AK3471 \
	--sample=CIP_BUK_ATTGGCGT,CIP_BUK_CGCCATCG,CIP_BUK_GCAACAAA,CIP_BUK_TAGTTGTC,NDHX00513 \
	--maxreads='all'

supernova mkoutput \
	--style=pseudohap2 \
	--asmdir=/home/ilaydab/scratch/renamed-libraries/x-l-libraries/outs/assembly/ \
	--outprefix=pseudohap2-bukasovii-assembly-output